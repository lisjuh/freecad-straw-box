# FreeCAD Straw Box

This repository contains the CAD design files for a self-designed straw box, created using FreeCAD. The straw box is designed to hold and dispense two individual straws, separated by a partition.

![Straw Box](./images/FreeCAD/StrawBox.png "Straw Box")

## Specifications

- Straw Box Dimensions:
  - 205mm H x 27mm W x 17mm D
  - wall thickness is 2mm and 1.6mm

- Straw Dimensions:
  - Diameter: 8mm
  - Length: 200mm

- Box Features:
  - Holds two straws
  - Straws are separated by a partition
  - Consists of two parts:
    1. Lid
    2. Base

## Design Files

The CAD design files are provided in the following formats:

- `StrawBox.fcstd` (FreeCAD native format)
- `StrawBox-Lid.stl` (STereoLithography file format of the lid for 3D printing)
- `StrawBox-Base.stl` (STereoLithography file format of the base for 3D printing)

## 3D Printer Settings

- Material: PLA (Polylactide)
- Infill: 15%
- Nozzle: 0.2mm
- Color: Orange
- Layer height: 0.16mm

## 3D Printed Straw Box

<img src="./images/ProductPhotos/StrawBox1.jpg" alt="3D printed Straw Box" width="1300" height="auto">

## Contributing

If you have any suggestions, improvements, or modifications to the design, please feel free to submit a pull request or open an issue in this repository.


## License

This project is licensed under the [Creative Commons Attribution-NonCommercial 4.0 International](https://creativecommons.org/licenses/by-nc/4.0/deed.en) license.
